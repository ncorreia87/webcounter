FROM python:slim
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . .
EXPOSE 5000
ARG REDIS_URL='localhost'
ENV REDIS_URL=${REDIS_URL}
CMD ["python", "app.py"]
