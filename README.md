Project Webcounter
==================

Simple Python webcounter
* author: __Nuno Correia__
* date: __2022-04-19__



## Build 

`docker build -t nunoc87/webcounter .`

## Dependencies
*Redis server: `REDIS_URL`

`docker run -d --name redis --rm
redis:alpine`

## Running 
`docker run -d --name webcounter --rm
-p 80:5000 --link redis webcounter`

## Running on stack

Create a swarm
`docker swarm init`

Join nodes to stack 

`docker stack deploy --compose-file docker-compose.yaml app`

Docker compose (if installed)

`docker compose up -d`